import { tns } from "tiny-slider/src/tiny-slider"

let slider = tns({
  container: '.home-carrousel-big',
  items: 1,
  controls: false,
  nav: false,
  autoplay: false,
  autoplayButtonOutput: false,
  autoplayTimeout: 1000
})

let sliderMobile = tns({
  container: '.home-carrousel-small',
  items: 1,
  controls: false,
  nav: false,
  autoplay: false,
  autoplayButtonOutput: false,
  autoplayTimeout: 1000
})

$('#carrouselPrevArrow').click(function () {
  slider.goTo('prev')
})

$('#carrouselNextArrow').click(function () {
  slider.goTo('next')
})

$('#carrouselMobilePrevArrow').click(function () {
  sliderMobile.goTo('prev')
})

$('#carrouselMobileNextArrow').click(function () {
  sliderMobile.goTo('next')
})